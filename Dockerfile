FROM java:8-jdk-alpine
COPY target/rest-api-1.0-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 9090
ENTRYPOINT ["java", "-jar", "rest-api-1.0-SNAPSHOT.jar"]